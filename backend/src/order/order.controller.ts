import {
    Controller,
    Post,
    Body,
    HttpCode,
    Get,
    Delete,
    NotFoundException,
  } from '@nestjs/common';
  import { ApiModelProperty } from '@nestjs/swagger';
  import { OrderService } from './order.service';
  
  @Controller('order')
  export class OrderController {
    constructor(private readonly service: OrderService) {}
  
    @Post('confirm')
    @HttpCode(201)
    async confirmOrder(@Body() body){
        const {userId, orderItems, total, smile, wallet, paid, deposit} = body;
        const response = await this.service.sendOrderConfirmation(userId, orderItems, total, smile, wallet, paid, deposit);
        if (!response) {
            throw new NotFoundException('Order Creation Failed');
          }
          return {
            statusCode: 201
          };
    }
  }
  