import { Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';

export type Order = {
  userId: string;
  orderItems: OrderItem[];
  total: number;
  smile: number;
  wallet: number;
  paid: number;
  deposit: number;
};

export type OrderItem = {
    name: string;
    price: number;
};

@Injectable()
export class OrderRepository {
  private readonly collection: Collection<Order>;

  constructor(private readonly db: Db) {
    this.collection = this.db.collection('order');
  }

  async createOrder(userId: string, orderItems: OrderItem[], total: number, smile: number, wallet: number, paid: number, deposit: number){
      return await this.collection.insertOne({ userId, orderItems, total, smile, wallet, paid, deposit});
  }
}
