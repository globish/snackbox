import { Injectable, BadRequestException } from '@nestjs/common';
import {
  WebhookEvent,
  TextMessage,
  Client,
  ImageMessage,
  FollowEvent,
  MessageEvent,
  FlexMessage,
  FlexBubble,
  FlexBubbleStyle,
  Message,
  FlexContainer,
} from '@line/bot-sdk';
import { OrderItem, OrderRepository } from './order.repository';

@Injectable()
export class OrderService {
  constructor(
    private readonly lineClient: Client,
    private readonly repo: OrderRepository,
  ) {}

  async sendOrderConfirmation(userId, orderItems, total, smile, wallet, paid, deposit){
      //add order and send confirmation
      //snackName
      //snackPrice
      let items: OrderItem[] = [];
      const receiptItems = [];
      orderItems.forEach( (item) => {
        items.push({name: item.snackName, price: item.snackPrice});
        receiptItems.push({
            "type": "box",
            "layout": "horizontal",
            "contents": [
              {
                "type": "text",
                "text": item.snackName,
                "size": "sm",
                "color": "#555555",
                "flex": 0
              },
              {
                "type": "text",
                "text": "฿"+item.snackPrice,
                "size": "sm",
                "color": "#111111",
                "align": "end"
              }
            ]
          });
      });
      receiptItems.push(
        {
          "type": "separator",
          "margin": "xxl"
        },
        {
          "type": "box",
          "layout": "horizontal",
          "margin": "xxl",
          "contents": [
            {
              "type": "text",
              "text": "ITEMS",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": ""+items.length,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "TOTAL",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "฿"+total,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "SMILE 😊",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "฿"+smile,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "CASH",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "฿"+paid,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "DEPOSIT",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "฿"+deposit,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        });
      let content: FlexContainer =  
        {
        "type": "bubble",
        "styles": {
          "footer": {
            "separator": true
          }
        },
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "RECEIPT",
              "weight": "bold",
              "color": "#1DB446",
              "size": "sm"
            },
            {
              "type": "text",
              "text": "Snack Box",
              "weight": "bold",
              "size": "xxl",
              "margin": "md"
            },
            {
              "type": "text",
              "text": "by Globish",
              "size": "xs",
              "color": "#aaaaaa",
              "wrap": true
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "xxl",
              "spacing": "sm",
              "contents": receiptItems
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "horizontal",
              "margin": "md",
              "contents": [
                {
                  "type": "text",
                  "text": "Thank you for using Snack Box. Please come again :)",
                  "size": "xs",
                  "color": "#aaaaaa",
                  "wrap": true,
                  "flex": 0
                }
              ]
            }
          ]
        }
      };
      let msg: FlexMessage = {
        type: "flex",
        altText: "This is a flex message",
        contents: content
    };
      await this.repo.createOrder(userId, items, total, smile, wallet, paid, deposit);
      await this.lineClient.pushMessage(userId, msg);
    return true;
  }

  private async handleMessageEvent(event: MessageEvent) {
    const {
      message,
      source: { userId },
    } = event;
    if (message.type === 'text') {
      const { text } = message;
      switch (text.toLowerCase()) {
        case 'qr': {
          await this.sendQR(userId);
          break;
        }
        case 'balance': {
          // TODO: wait for check balance
          break;
        }
        case 'stock': {
          const msg: TextMessage = {
            text: 'อยู่แค่นี้เอง เดินมาดูก็ได้',
            type: 'text',
          };

          await this.lineClient.pushMessage(userId, msg);
          break;
        }
        case 'request': {
          const msg: TextMessage = {
            text: 'ถามไปงั้นแหละ ไม่เพิ่มให้หรอก',
            type: 'text',
          };

          await this.lineClient.pushMessage(userId, msg);
          break;
        }
      }
    }
  }

  private async sendQR(userId: string) {
    const qrUrl = this.getQRUrl(userId);

    const imageMsg: ImageMessage = {
      originalContentUrl: qrUrl,
      previewImageUrl: qrUrl,
      type: 'image',
    };

    await this.lineClient.pushMessage(userId, imageMsg);
  }

  private getFilename(userId: string) {
    const fileName = `${userId}.png`;
    return fileName;
  }

  private getQRUrl(userId: string) {
    const fileName = this.getFilename(userId);
    const qrUrl = `https://snackbox.globish.co.th/api/${fileName}`;
    return qrUrl;
  }
}
