import { Module } from '@nestjs/common';
import { GlobalModule } from './global.module';
import { TodoModule } from './todo/todo.module';
import { LineModule } from './line/line.module';
import { ConfigModule } from './config/config.module';
import { WalletModule } from './wallet/wallet.module';
import { SnackModule } from './snack/snack.module';
import { OrderModule } from './order/order.module';

@Module({
  imports: [GlobalModule, TodoModule, LineModule, ConfigModule, SnackModule, WalletModule, OrderModule],
  controllers: [],
  providers: [],
})
export class AppModule {}


