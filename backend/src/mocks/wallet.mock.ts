// ./src/mocks/wallet.mock.ts
export const WALLETS = [
  { id: 1, balance: 200, owner: 'Tan' },
  { id: 2, balance: 200, owner: 'Mook' },
  { id: 3, balance: 200, owner: 'Aof ' },
  { id: 4, balance: 200, owner: 'Meow' },
  { id: 5, balance: 200, owner: 'Kriang' },
  { id: 6, balance: 200, owner: 'Nai' },
];
