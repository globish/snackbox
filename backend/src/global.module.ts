import { Module, Global } from '@nestjs/common';
import { MongoClient, Db } from 'mongodb';
import { Client, ClientConfig } from '@line/bot-sdk';
import { ConfigService } from './config/config.service';

const config = new ConfigService(
  `.env.${process.env.NODE_ENV || 'development'}`,
);

const mongoProvider = {
  provide: Db,
  useFactory: async () => {
    const url = `mongodb://${config.get('DB_HOST')}:${config.get('DB_PORT')}`;
    const dbName = config.get('DB_NAME');
    const client = new MongoClient(url);
    const connectedClient = await client.connect();
    const db = connectedClient.db(dbName);
    return db;
  },
};

const lineSdkProvider = {
  provide: Client,
  useFactory: async () => {
    const clientConfig: ClientConfig = {
      channelAccessToken:
        'W1gCI/lS4pUdw+IKLydhfeWbggqCDMuhyuQ6znEQj7RPP9Hv4j8cctvFK2pOTvdy1qxO6ira37sbd8H4jOhB3iooDqV5xgv4qO1grHPm8jzkFE8aF2340N3c7iZgxq+xZceeusYoaTsVlDlGOC5MhQdB04t89/1O/w1cDnyilFU=',
      channelSecret: '5986106d1c3618fa9f943cb15ab18447',
    };
    const client = new Client(clientConfig);
    return client;
  },
};


@Global()
@Module({
  imports: [],
  controllers: [],
  providers: [mongoProvider, lineSdkProvider],
  exports: [mongoProvider, lineSdkProvider],
})
export class GlobalModule {}
