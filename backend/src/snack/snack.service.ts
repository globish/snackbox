import { Injectable } from '@nestjs/common';
import { SnackRepository } from './snack.repository';

@Injectable()
export class SnackService {
  constructor(private readonly repo: SnackRepository) {}

  async getSnack() {
    return await this.repo.getSnack();
  }
  async createSnack(snackName: string,snackBarcode: string,snackPrice: string) {
    const numPrice = parseInt(snackPrice);
    await this.repo.createSnack(snackName, snackBarcode, numPrice);
  }
  async removeSnack(id:string){
    return await this.repo.removeSnack(id)
  }
}


