import { Module } from '@nestjs/common';
import { SnackController } from './snack.controller';
import { SnackService } from './snack.service';
import { SnackRepository } from './snack.repository';

@Module({
  imports: [],
  controllers: [SnackController],
  providers: [SnackService, SnackRepository],
})
export class SnackModule {}
