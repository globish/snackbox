import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { SnackService } from './snack.service';
import { ApiModelProperty } from '@nestjs/swagger';
import { identity } from 'rxjs';
import { async } from 'rxjs/internal/scheduler/async';

export class CreateSnack {
  @ApiModelProperty()
  public snackName: string;

  @ApiModelProperty()
  public snackBarcode : string;

  @ApiModelProperty()
  public snackPrice : string;

  @ApiModelProperty()
  public id : string;
}

@Controller('snack')
export class SnackController {
  constructor(private readonly snackService: SnackService) {}

  @Get()
  async getSnack() {
    return await this.snackService.getSnack();
  }

  @Post()
    async createSnack(@Body() body: CreateSnack) {
    const { snackName, snackBarcode, snackPrice } = body;
    await this.snackService.createSnack(snackName, snackBarcode, snackPrice);
    return { ok: 'ok' };
  }

  @Delete(':_id')
  async removeSnack(@Param('_id') id:string) {   
    const result = await this.snackService.removeSnack(id);
    return { ok: 'ok', result: result, deletingId: id };
  }
}