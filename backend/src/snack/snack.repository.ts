import { Injectable } from '@nestjs/common';
import { Db, Collection, ObjectId } from 'mongodb';

export type Snack = {
  snackName: string;
  snackBarcode: string;
  snackPrice: number;
};

@Injectable()
export class SnackRepository {
  private readonly collection: Collection<Snack>;

  constructor(private readonly db: Db) {
    this.collection = this.db.collection('snack');
  }

  async getSnack() {
    return await this.collection.find().toArray();
  }
  async removeSnack(_id: string){
    return await this.collection.remove( { "_id":new ObjectId(_id)});
  }
  async createSnack(
    snackName: string,
    snackBarcode: string,
    snackPrice: number,
  ) {
    await this.collection.updateOne(
      { snackBarcode },
      { $set: { snackName, snackBarcode, snackPrice } },
      { upsert: true },
    );
  }
}