import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
} from '@nestjs/common';
import { WalletService } from './wallet.service';
import { CreateWalletDTO } from './dto/create-wallet.dto';
import * as fs from 'fs';
import * as path from 'path';
import * as uuid from 'uuid';

@Controller('wallet')
export class WalletController {
  constructor(private walletService: WalletService) {}
  @Get()
  async getWallets() {
    const wallet = await this.walletService.getWallets();
    return wallet;
  }

  @Get(':userId')
  async getWallet(@Param('userId') userId: string) {
    const wallet = await this.walletService.getWallet(userId);
    return wallet;
  }

  @Post()
  async createWallet(@Body() createWalletDTO: CreateWalletDTO) {
    const { userId, balance, owner } = createWalletDTO;
    const wallet = await this.walletService.createWallet(
      userId,
      balance,
      owner,
    );
    return wallet;
  }

  @Put(':userId')
  async updateWallet(
    @Param('userId') userId: string,
    @Body() createWalletDTO: CreateWalletDTO,
  ) {
    const { balance, owner } = createWalletDTO;
    const wallet = await this.walletService.updateWallet(
      userId,
      balance,
      owner,
    );
    return wallet;
  }
  
  private blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return theBlob as File;
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('filename'))
  async uploadFile(@UploadedFile() file) {
    console.log(file);
    const dest = path.resolve(__dirname, '../../public/smiley', uuid() + '.png');

    const promise = new Promise((resolve, reject) => {
      fs.createWriteStream(dest).write(file.buffer, err => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
    try {
      await promise;
    } catch (e) {
      console.log('error krub', e);
    }
    console.log('awaited');
  }
}
