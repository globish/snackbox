import { Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';

@Injectable()
export class WalletRepository {
  private readonly collection: Collection;

  constructor(private readonly db: Db) {
    this.collection = this.db.collection('wallet');
  }

  async getWallets() {
    return await this.collection.find().toArray();
  }

  async getWallet(userId) {
    return await this.collection.findOne({ userId });
  }

  async createWallet(userId: string, balance: number, owner: string) {
    return await this.collection.insert({ userId, balance, owner });
  }

  async updateWallet(userId: string, balance: number, owner: string) {
    return await this.collection.update({ userId }, { userId, balance, owner }, {upsert: true});
  }
}
