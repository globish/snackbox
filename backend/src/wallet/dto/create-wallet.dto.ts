import { ApiModelProperty } from '@nestjs/swagger';

export class CreateWalletDTO {
  @ApiModelProperty()
  readonly userId: string;
  @ApiModelProperty()
  readonly balance: number;
  @ApiModelProperty()
  readonly owner: string;
}
