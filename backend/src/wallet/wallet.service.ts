import { Injectable, HttpException } from '@nestjs/common';
import { WalletRepository } from './wallet.repository';

@Injectable()
export class WalletService {
  constructor(private readonly repo: WalletRepository) {}

  async getWallets() {
    return await this.repo.getWallets();
  }

  async getWallet(userId: string) {
    const wallet = await this.repo.getWallet(userId);
    if (!wallet) {
      return {
        userId,
        balance: 0,
        owner: null,
      };
    }
    return wallet;
  }

  async createWallet(userId: string, balance: number, owner: string) {
    return await this.repo.createWallet(userId, balance, owner);
  }

  async updateWallet(userId: string, balance: number, owner: string) {
    return await this.repo.updateWallet(userId, balance, owner);
  }
}
