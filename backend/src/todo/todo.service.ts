import { Injectable } from '@nestjs/common';
import { TodoRepository } from './todo.repository';

@Injectable()
export class TodoService {
  constructor(private readonly repo: TodoRepository) {}

  async getTodo() {
    return await this.repo.getTodo();
  }
  async createTodo(name: string, description: string) {
    await this.repo.createTodo(name, description);
  }
}
