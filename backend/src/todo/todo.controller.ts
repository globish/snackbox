import { Controller, Get, Post, Body } from '@nestjs/common';
import { TodoService } from './todo.service';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateTodoDto {
  @ApiModelProperty()
  public readonly name: string;

  @ApiModelProperty()
  public readonly description: string;
}

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  async getTodo() {
    return await this.todoService.getTodo();
  }

  @Post()
  async createTodo(@Body() body: CreateTodoDto) {
    const { name, description } = body;
    await this.todoService.createTodo(name, description);
    return { ok: 'ok' };
  }
}
