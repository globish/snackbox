import { Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';

@Injectable()
export class TodoRepository {
  private readonly collection: Collection;

  constructor(private readonly db: Db) {
    this.collection = this.db.collection('todo');
  }

  async getTodo() {
    return await this.collection.find().toArray();
  }
  async createTodo(name: string, description: string) {
    await this.collection.insert({ name, description });
  
  }
}
