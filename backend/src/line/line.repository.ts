import { Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';

export type User = {
  userId: string;
  otp: string;
};

@Injectable()
export class LineRepository {
  private readonly collection: Collection<User>;

  constructor(private readonly db: Db) {
    this.collection = this.db.collection('line');
  }

  async createLineUser(userId: string, otp: string) {
    return await this.collection.insertOne({ userId, otp });
  }

  async getUser(userId: string) {
    return await this.collection.findOne({ userId });
  }

  async getUserByOtp(otp: string) {
    return await this.collection.findOne({ otp });
  }

  async clearUserOtp(userId: string) {
    return await this.collection.updateOne({ userId }, { $set: { otp: null } });
  }

  async getAllUsers(): Promise<User[]> {
    return await this.collection.find().toArray();
  }

  async deleteAllUsers() {
    await this.collection.deleteMany({});
  }
}
