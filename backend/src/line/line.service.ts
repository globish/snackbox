import { Injectable, BadRequestException } from '@nestjs/common';
import { LineRepository, User } from './line.repository';
import {
  WebhookEvent,
  TextMessage,
  Client,
  ImageMessage,
  FollowEvent,
  MessageEvent,
} from '@line/bot-sdk';
import { generateOtp } from './otp';
import * as QRCode from 'qrcode';
import * as path from 'path';
import { WalletService } from '../wallet/wallet.service';

@Injectable()
export class LineService {
  constructor(
    private readonly walletService: WalletService,
    private readonly repo: LineRepository,
    private readonly lineClient: Client,
  ) {}

  private async generateNonExistingOtp(): Promise<string> {
    let otp: string;
    let otpUser;
    do {
      otp = generateOtp();
      otpUser = await this.repo.getUserByOtp(otp);
    } while (otpUser !== null);
    return otp;
  }

  async handleEvents(events: WebhookEvent[]) {
    for (const event of events) {
      if (event.type === 'follow') {
        await this.handleFollowEvent(event);
      } else if (event.type === 'message') {
        await this.handleMessageEvent(event);
      }
    }
  }

  private async handleFollowEvent(event: FollowEvent) {
    const {
      replyToken,
      source: { userId },
    } = event;

    const otp = await this.generateNonExistingOtp();

    const existingUser = await this.repo.getUser(userId);
    if (existingUser) {
      return;
    }

    await this.repo.createLineUser(userId, otp);

    const textMsg: TextMessage = {
      text: otp,
      type: 'text',
    };
    await this.lineClient.replyMessage(replyToken, textMsg);

    const fileName = this.getFilename(userId);

    await this.generateQR(userId, fileName);
  }

  private async handleMessageEvent(event: MessageEvent) {
    const {
      message,
      source: { userId },
    } = event;
    if (message.type === 'text') {
      const { text } = message;
      switch (text.toLowerCase()) {
        case 'qr': {
          await this.sendQR(userId);
          break;
        }
        case 'balance': {
          const wallet = await this.walletService.getWallet(userId);
          const { balance } = wallet;
          const msg: TextMessage = {
            text: `เงินสะสมของคุณเหลือ ${balance} บาท`,
            type: 'text',
          };
          await this.lineClient.pushMessage(userId, msg);
          break;
        }
        case 'stock': {
          const msg: TextMessage = {
            text: 'อยู่แค่นี้เอง เดินมาดูก็ได้',
            type: 'text',
          };

          await this.lineClient.pushMessage(userId, msg);
          break;
        }
        case 'request': {
          const msg: TextMessage = {
            text: 'ถามไปงั้นแหละ ไม่เพิ่มให้หรอก',
            type: 'text',
          };

          await this.lineClient.pushMessage(userId, msg);
          break;
        }
      }
    }
  }

  private async sendQR(userId: string) {
    const qrUrl = this.getQRUrl(userId);

    const imageMsg: ImageMessage = {
      originalContentUrl: qrUrl,
      previewImageUrl: qrUrl,
      type: 'image',
    };

    await this.lineClient.pushMessage(userId, imageMsg);
  }

  private getFilename(userId: string) {
    const fileName = `${userId}.png`;
    return fileName;
  }

  private getQRUrl(userId: string) {
    const fileName = this.getFilename(userId);
    const qrUrl = `https://snackbox.globish.co.th/api/${fileName}`;
    return qrUrl;
  }

  async generateQR(data: string, fileName: string) {
    const filePath = path.join(__dirname, '../../public', fileName);

    await QRCode.toFile(filePath, data);
  }

  async checkOtp(otp: string): Promise<string> {
    const otpUser = await this.repo.getUserByOtp(otp);
    if (!otpUser) {
      throw new BadRequestException('This otp is invalid');
    }
    const { userId } = otpUser;

    await this.repo.clearUserOtp(userId);

    const textMsg: TextMessage = {
      text: 'Use your QR Code below to login',
      type: 'text',
    };

    await this.lineClient.pushMessage(userId, textMsg);

    await this.sendQR(userId);

    return userId;
  }

  async getAllUsers(): Promise<User[]> {
    return await this.repo.getAllUsers();
  }

  async deleteAllUsers() {
    return await this.repo.deleteAllUsers();
  }

  async verifyUserId(userId: string): Promise<boolean> {
    const user = await this.repo.getUser(userId);
    return !!user;
  }
}
