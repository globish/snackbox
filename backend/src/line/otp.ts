export function generateOtp() {
  return Math.random()
    .toString()
    .substr(2, 6);
}
