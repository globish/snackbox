import {
  Controller,
  Post,
  Body,
  HttpCode,
  Get,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { ApiModelProperty } from '@nestjs/swagger';
import { WebhookEvent } from '@line/bot-sdk';
import { LineService } from './line.service';
import { User } from './line.repository';

export class CheckOtpDto {
  @ApiModelProperty()
  public readonly otp: string;
}

export class VerfiyUserDto {
  @ApiModelProperty()
  public readonly userId: string;
}

@Controller('line')
export class LineController {
  constructor(private readonly service: LineService) {}

  @Post()
  @HttpCode(200)
  async onLineEventTriggered(@Body() body: { events: WebhookEvent[] }) {
    const { events } = body;
    console.log(events);
    await this.service.handleEvents(events);
  }

  @Get()
  async getUsers(): Promise<User[]> {
    return await this.service.getAllUsers();
  }

  @Delete()
  async deleteUsers() {
    return await this.service.deleteAllUsers();
  }

  @Post('otp')
  @HttpCode(200)
  async checkOtp(@Body() body: CheckOtpDto): Promise<{ userId: string }> {
    const { otp } = body;
    const userId = await this.service.checkOtp(otp);
    return { userId };
  }

  @Get('health')
  async healthcheck() {
    return 'health ok';
  }

  @Get('qr')
  async saveQR() {
    await this.service.generateQR('hello', 'hello.png');
  }

  @Post('verify')
  @HttpCode(200)
  async verifyUser(@Body() body: VerfiyUserDto) {
    const { userId } = body;
    const isOk = await this.service.verifyUserId(userId);
    if (!isOk) {
      throw new NotFoundException('Cannot found user with this id');
    }
    return {
      statusCode: 200,
    };
  }
}
