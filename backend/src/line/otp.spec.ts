import { generateOtp } from './otp';

describe('generate otp', () => {
  test('single otp', () => {
    const result: string = generateOtp();
    console.log(result);
    expect(result.length).toEqual(6);
  });
});
