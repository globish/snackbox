import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')

    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/register',
      name: 'register',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "register" */ './views/Register.vue')
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/scan',
      name: 'scan',
      component: () => import(/* webpackChunkName: "scan" */ './views/Scan.vue')
    },
    {
      path: '/coin',
      name: 'coin',
      component: () => import(/* webpackChunkName: "coin" */ './views/Coin.vue')
    },
    {
      path: '/smile',
      name: 'smile',
      component: () => import(/* webpackChunkName: "smile" */ './views/Smile.vue')
    },
    {
      path: '/add',
      name: 'add',
      component: () => import(/* webpackChunkName: "add" */ './views/AddSnack.vue')
    },
    {
      path: '/thankyou',
      name: 'thankyou',
      component: () => import(/* webpackChunkName: "thankyou" */ './views/Thankyou.vue')
    }
  ]
})
