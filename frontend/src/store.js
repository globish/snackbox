import Vue from 'vue';
import Vuex from 'vuex';
import pathify from 'vuex-pathify';
import { make } from 'vuex-pathify';

Vue.use(Vuex);

const state = {
  user: null,
  menu: 'login',
  loggedInUser: null,
  barCode: null,
  snackInfo: null,
  snackOrder: [],
  scanMenu: 'proceed',
  orderInfo: {},
  isSmile: false
};

const getters = {
  ...make.getters(state),
  totalPrice: state => {
    let sum = 0;
    state.snackOrder.forEach(function(snackOrder) {
      sum += snackOrder.snackPrice;
    });
    return sum;
  },
  discountedPrice: (state, getters) => {
    let discountedPrice = 0;
    if (state.isSmile) {
      discountedPrice = getters.totalPrice - 3;
    }
    return discountedPrice;
  }
};

const mutations = {
  ...make.mutations(state)
};

const actions = {
  ...make.actions(state)
};

export default new Vuex.Store({
  plugins: [pathify.plugin],
  state,
  getters,
  mutations,
  actions
});
