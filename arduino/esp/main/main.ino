#include <WiFi.h> 
#include <Wire.h>
#include "FirebaseESP32.h"
#include "LiquidCrystal_I2C.h"
#include "esp_wpa2.h"

#define FIREBASE_HOST "snackbox-16154.firebaseio.com"
#define FIREBASE_AUTH "RgiFPhvIrd0ko1zW3VFCSGNNUVYQ0ELoTzhU1IyJ"

#define WIFI_SSID "Tou-ChI Hotspot"
#define WIFI_PASSWORD "falalalala"

// #define WIFI_SSID "LPIIB1"
// #define EAP_ID ""
// #define EAP_USERNAME ""
// #define EAP_PASSWORD ""

#define LCD_N_COLUMN 16
#define LCD_N_ROW 2

#define COIN 4
#define PULSE_TIMEOUT_MS 200 // In fast mode, each pulse is 20ms, with an interval of 100ms

#define LED_BUILTIN 2
#define RESET 3
#define LED_EXT 5

FirebaseData fdAmount;
FirebaseData fdCurrentAmount;
FirebaseData fdStart;
FirebaseData fdEnd;

int prevCreditCount = 0;
volatile int creditCount = 0;
volatile long lastCreditChangeTime = 0;

// Set LCD address, number of columns and rows
LiquidCrystal_I2C lcd(0x27, LCD_N_COLUMN, LCD_N_ROW);

enum statusSpace {
  CLEAR,
  IDLE,
  GET_AMOUNT,
  WAIT_FOR_COINS,
  END
} status;

unsigned long amount = 0;

void printLCD(char* line1, char* line2) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line1);
  
  lcd.setCursor(0, 1);
  lcd.print(line2);
}

void initLCD() {
  lcd.init();
  lcd.backlight();

  printLCD("SNACKBOX ORDER", "WELCOME");
}

void blinkLed() {
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
}

void onCoinPulse() {
  if(status != WAIT_FOR_COINS) return;
  Serial.println("!!!");
  // Make sure this is a real pulse and not a spurious glitch
  delayMicroseconds(3000); // 3ms
  if(digitalRead(COIN) == LOW) {
    creditCount += 1;
    lastCreditChangeTime = millis();
  } else {
    Serial.println("Glitch");
  }
}

void initCoinAcceptor() {
  pinMode(COIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(COIN), onCoinPulse, FALLING); 
}

void initResetButton() {
  pinMode(RESET, INPUT);
}

void initExtLed() {
  pinMode(LED_EXT, OUTPUT);
  digitalWrite(LED_EXT, LOW);
}

void setup() {
  Serial.begin(115200);
  delay(10);

  status = IDLE;

  initLCD();
  initCoinAcceptor();
  initResetButton();
  initExtLed();

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // Connect to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

//  WPA2 enterprise magic starts here
//  WiFi.disconnect(true);
//  WiFi.mode(WIFI_STA);
//  esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)EAP_ID, strlen(EAP_ID));
//  esp_wifi_sta_wpa2_ent_set_username((uint8_t *)EAP_USERNAME, strlen(EAP_USERNAME));
//  esp_wifi_sta_wpa2_ent_set_password((uint8_t *)EAP_PASSWORD, strlen(EAP_PASSWORD));
//  esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();
//  esp_wifi_sta_wpa2_ent_enable(&config);
//  WPA2 enterprise magic ends here

//  WiFi.begin(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while(WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);

  if(!Firebase.beginStream(fdStart, "/Start")) {
    Serial.println("FAILED");
    Serial.println("REASON: " + fdStart.errorReason());
    Serial.println();
  }
}

void loop() {

  // Watch /Start
  if(!Firebase.readStream(fdStart)) {
    Serial.println("------------------------------------");
    Serial.println("Read stream");
    Serial.println("FAILED");
    Serial.println("REASON: " + fdStart.errorReason());
    Serial.println();
  }

  if(fdStart.streamTimeout()) {
    Serial.println("Stream timeout, resume streaming...");
    Serial.println();
  }

  if(fdStart.streamAvailable()) {
    Serial.println("Stream Data available...");
    Serial.println("STREAM PATH: " + fdStart.streamPath());
    Serial.println("PATH: " + fdStart.dataPath());
    Serial.println("TYPE: " + fdStart.dataType());
    Serial.print("VALUE: ");
    if(fdStart.dataType() == "int") {
      Serial.println(fdStart.intData());
      int startSignal = fdStart.intData();
      if(startSignal == 1 && status == IDLE) {
        status = GET_AMOUNT;
        Serial.println("Start");
      }
      if(startSignal == 0 && status != IDLE) {
        status = CLEAR;
      }
    }
    Serial.println();
  }

  if(status == CLEAR) {
    // Print to LCD
    printLCD("SNACKBOX ORDER", "WELCOME");
    status = IDLE;

    digitalWrite(LED_EXT, LOW);

    amount = 0;
    creditCount = 0;
    prevCreditCount = 0;
    lastCreditChangeTime = 0;
  }

  // Status: GET_AMOUNT
  if(status == GET_AMOUNT) {
    Serial.println("Status: GET_AMOUNT");
    if(Firebase.getInt(fdAmount, "/Amount")) {
      Serial.println("PASSED");
      Serial.println("PATH: " + fdAmount.dataPath());
      Serial.println("TYPE: " + fdAmount.dataType());
      Serial.print("VALUE: ");
      if(fdAmount.dataType() == "int") {
        Serial.println(fdAmount.intData());
        amount = fdAmount.intData();

        // Print to LCD
        char line1[16], line2[16];
        sprintf(line1, "PRICE: %d BAHT", amount);
        sprintf(line2, "PAID: %d BAHT", creditCount);
        printLCD(line1, line2);
        
        status = WAIT_FOR_COINS;
        digitalWrite(LED_EXT, HIGH);
      }
      Serial.println();
    }
    blinkLed();
  }

  // Status: WAIT_FOR_COINS
  if(status == WAIT_FOR_COINS) {
//    Serial.println("Status: WAIT_FOR_COINS");
    if(creditCount > prevCreditCount) {
      // Print to LCD
      char line1[16], line2[16];
      sprintf(line1, "PRICE: %d BAHT", amount);
      sprintf(line2, "PAID: %d BAHT", creditCount);
      printLCD(line1, line2);
    }
    if(creditCount > prevCreditCount && millis() - lastCreditChangeTime > PULSE_TIMEOUT_MS) {
      if(creditCount >= amount) {
        if(Firebase.setInt(fdCurrentAmount, "/CurrentAmount", creditCount)) {
          Serial.println("PASSED");
          Serial.println("PATH: " + fdCurrentAmount.dataPath());
          Serial.println("TYPE: " + fdCurrentAmount.dataType());
          Serial.print("VALUE: ");
          if (fdCurrentAmount.dataType() == "int") {
            Serial.println(fdCurrentAmount.intData());
          }
          Serial.println();
          prevCreditCount = creditCount;
        }
        else {
          Serial.println("FAILED");
          Serial.println("REASON: " + fdEnd.errorReason());
          Serial.println();
        }
        
        status = END;
      }

      blinkLed();
    }
  }

  // Status: END
  if(status == END) {
    Serial.println("Status: END");
    digitalWrite(LED_EXT, LOW);
    if(Firebase.setInt(fdEnd, "/Start", 2)) {
      Serial.println("PASSED");
      Serial.println("PATH: " + fdEnd.dataPath());
      Serial.println("TYPE: " + fdEnd.dataType());
      Serial.print("VALUE: ");
      if (fdEnd.dataType() == "int") {
        Serial.println(fdEnd.intData());
        if(fdEnd.intData() == 2) {
          status = CLEAR;
        }
      }
      Serial.println();
    }
    else
    {
      Serial.println("FAILED");
      Serial.println("REASON: " + fdEnd.errorReason());
      Serial.println();
    }
  }
}
