/*
ESP8266 Blink
Blink the blue LED on the ESP8266 module
*/
#define LED 2 //Define blinking LED pin
void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT); // Initialize the LED pin as an output
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED, LOW); // Turn the LED on (Note that LOW is the voltage level)
  delay(1000); // Wait for a second
  digitalWrite(LED, HIGH); // Turn the LED off by making the voltage HIGH
  delay(1000); // Wait for two seconds
}
